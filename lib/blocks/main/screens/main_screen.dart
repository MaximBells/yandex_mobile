import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/main/data/main_controller.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainController>(
        init: Get.find<MainController>(),
        builder: (controller) {
          return Scaffold(
              body: controller.activeScreen,
              bottomNavigationBar: BottomNavigationBar(
                backgroundColor: Colors.blue,
                selectedItemColor: Colors.white,
                unselectedItemColor: Colors.white70,
                currentIndex: controller.activeIndex,
                onTap: (index) => controller.updateIndex(index),
                items: const [
                  BottomNavigationBarItem(
                      icon: Icon(Icons.translate), label: 'Translate'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.visibility), label: 'Vision')
                ],
              ));
        });
  }
}
