import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/translate/screens/tranlsate_screen.dart';
import 'package:yandex_mobile/blocks/vision/screens/vision_screen.dart';
import 'package:yandex_mobile/components/services/camera_service.dart';

class MainController extends GetxController {
  int activeIndex = 0;

  Widget get activeScreen {
    if (activeIndex == 0) {
      return const TranslateScreen();
    } else {
      return Get.find<CameraService>().hasCamera
          ? VisionScreen(camera: Get.find<CameraService>().camera)
          : const Scaffold(
              body: Center(
              child: Text('no camera'),
            ));
    }
  }

  void updateIndex(int value) {
    activeIndex = value;
    update();
  }
}
