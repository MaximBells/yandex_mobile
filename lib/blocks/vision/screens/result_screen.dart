import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:yandex_mobile/blocks/vision/data/vision_controller.dart';
import 'package:yandex_mobile/components/models/vision_response.dart';

class ResultScreen extends StatelessWidget {
  const ResultScreen({Key? key, required this.visionResponse}) : super(key: key);
  final VisionResponse visionResponse;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: visionResponse.sentences.map((sentence) {
                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 12),
                    child: SizedBox(
                      width: Get.width,
                      child: Text(sentence.text),
                    ),
                  );
                }).toList(),
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 36, vertical: 12),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(24))),
                  onPressed: () {
                    Get.find<VisionController>().translateVision(visionResponse);
                  },
                  child: Text('Перевести'))
            ],
          ),
        ),
      ),
    );
  }
}
