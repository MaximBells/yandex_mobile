import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/vision/data/vision_controller.dart';
import 'package:yandex_mobile/blocks/vision/screens/empty_screen.dart';
import 'package:yandex_mobile/blocks/vision/screens/result_screen.dart';
import 'package:yandex_mobile/blocks/vision/widgets/loading_vision_widget.dart';
import 'package:yandex_mobile/components/services/request_service.dart';
import 'package:yandex_mobile/static/utils.dart';

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({super.key, required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Отобразить фото')),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Stack(
        alignment: Alignment.center,
        children: [
          SizedBox(
              height: Get.height,
              width: Get.width,
              child: Image.file(File(imagePath))),
          Container(
              margin: EdgeInsets.only(top: Get.height * 0.55),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 48, vertical: 12),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(24))),
                  onPressed: () {
                    Get.find<VisionController>().vision(imagePath);
                  },
                  child: const Text("ОТПРАВИТЬ"))),
          const LoadingVisionWidget()
        ],
      ),
    );
  }
}
