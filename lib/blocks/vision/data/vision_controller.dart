import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/main/data/main_controller.dart';
import 'package:yandex_mobile/blocks/translate/data/translate_controller.dart';
import 'package:yandex_mobile/blocks/vision/screens/empty_screen.dart';
import 'package:yandex_mobile/blocks/vision/screens/result_screen.dart';
import 'package:yandex_mobile/components/extensions/string_extension.dart';
import 'package:yandex_mobile/components/models/sentence.dart';
import 'package:yandex_mobile/components/models/vision_response.dart';
import 'package:yandex_mobile/components/services/request_service.dart';
import 'package:yandex_mobile/static/utils.dart';

class VisionController extends GetxController {
  bool _loading = false;

  bool get loading => _loading;

  Future<void> vision(String imagePath) async {
    loading = true;
    final response =
        await Get.find<RequestService>().visionRequest(imagePath.content);
    if (response == null) {
      return;
    }
    loading = false;
    Navigator.pushReplacement(
        Get.context!,
        CupertinoPageRoute(
            builder: (context) => response.sentences.isEmpty
                ? const EmptyScreen()
                : ResultScreen(visionResponse: response)));
  }

  void translateVision(VisionResponse response) {
    Get.find<TranslateController>().targetController.clear();
    Get.find<TranslateController>().targetController.text = response.parsedText;
    Navigator.pop(Get.context!);
    Get.find<MainController>().updateIndex(0);
  }

  set loading(bool value) {
    _loading = value;
    update();
  }
}
