import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/translate/data/translate_controller.dart';

class LanguageWidget extends StatefulWidget {
  const LanguageWidget({Key? key}) : super(key: key);

  @override
  State<LanguageWidget> createState() => _LanguageWidgetState();
}

class _LanguageWidgetState extends State<LanguageWidget> {
  String _value = "";

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 16),
      child: DropdownButton<String>(
        value: _value.isEmpty
            ? Get.find<TranslateController>().languages.first
            : _value,
        dropdownColor: Colors.blue,
        underline: Container(),
        icon: Icon(
          Icons.arrow_drop_down_outlined,
          color: Colors.blue,
        ),
        items: Get.find<TranslateController>().languages.map((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
              style: TextStyle(color: Colors.white),
            ),
          );
        }).toList(),
        onChanged: (value) {
          setState(() {
            if (value != null) {
              _value = value;
              Get.find<TranslateController>().activeTargetLanguage = _value;
            }
          });
        },
      ),
    );
  }
}
