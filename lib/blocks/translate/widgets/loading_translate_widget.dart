import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/translate/data/translate_controller.dart';

class LoadingTranslateWidget extends StatelessWidget {
  const LoadingTranslateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TranslateController>(
        init: Get.find<TranslateController>(),
        builder: (controller) {
          return AnimatedSwitcher(
            duration: const Duration(milliseconds: 500),
            child: controller.loading == false
                ? Container()
                : Stack(
                    alignment: Alignment.center,
                    children: [
                      BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 4.0, sigmaY: 4.0),
                        child: Opacity(
                          opacity: 0.8,
                          child: SizedBox(
                            width: Get.width,
                            height: Get.height,
                            child: const ModalBarrier(
                                dismissible: false, color: Colors.black),
                          ),
                        ),
                      ),
                      const CircularProgressIndicator()
                    ],
                  ),
          );
        });
  }
}
/*
AnimatedCrossFade(
              firstChild: Container(),
              secondChild: Stack(
                alignment: Alignment.center,
                children: [
                  BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 4.0, sigmaY: 4.0),
                    child: Opacity(
                      opacity: 0.8,
                      child: SizedBox(
                        width: Get.width,
                        height: Get.height,
                        child: const ModalBarrier(
                            dismissible: false, color: Colors.black),
                      ),
                    ),
                  ),
                  const CircularProgressIndicator()
                ],
              ),
              crossFadeState: controller.loading
                  ? CrossFadeState.showSecond
                  : CrossFadeState.showFirst,
              duration: const Duration(milliseconds: 500));
 */
