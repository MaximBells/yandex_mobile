import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/components/services/request_service.dart';
import 'package:yandex_mobile/components/services/token_service.dart';
import 'package:yandex_mobile/static/app_string.dart';
import 'package:yandex_mobile/static/utils.dart';

class TranslateController extends GetxController {
  final TextEditingController targetController = TextEditingController();

  final TextEditingController networkController = TextEditingController();

  List<String> languages = ['ru', 'en', 'fr', 'de'];

  bool _loading = false;

  bool get loading => _loading;

  set loading(bool value) {
    _loading = value;
    update();
  }

  String _activeLanguage = "";

  String _networkLanguage = "";

  String get activeTargetLanguage =>
      _activeLanguage.isEmpty ? languages.first : _activeLanguage;

  String get networkLanguage => switch (_networkLanguage) {
        'ru' => 'Русский',
        'en' => 'Английский',
        'fr' => 'Французский',
        _ => 'Немецкий',
      };

  String get targetLanguage => switch (activeTargetLanguage) {
        'ru' => 'Русский',
        'en' => 'Английский',
        'fr' => 'Французский',
        _ => 'Немецкий',
      };

  set activeTargetLanguage(String value) {
    _activeLanguage = value;
    update();
  }

  set networkLanguage(String value) {
    _networkLanguage = value;
    update();
  }

  Future<void> translate() async {
    loading = true;
    var translating = await Get.find<RequestService>()
        .translateRequest(targetController.text, activeTargetLanguage);
    logPrint(translating);
    if (translating != null && translating.isNotEmpty) {
      var translations = translating['translations'];
      if (translations is List) {
        var text = translations.first['text'];
        var detectedLanguage = translations.first['detectedLanguageCode'];
        networkController.text = text ?? "";
        networkLanguage = detectedLanguage ?? "";
      }
    }
    loading = false;
  }
}

/*

 */
