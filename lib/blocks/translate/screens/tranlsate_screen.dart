import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/translate/data/translate_controller.dart';
import 'package:yandex_mobile/blocks/translate/widgets/language_widget.dart';
import 'package:yandex_mobile/blocks/translate/widgets/loading_translate_widget.dart';

class TranslateScreen extends StatelessWidget {
  const TranslateScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TranslateController>(
        init: Get.find<TranslateController>(),
        builder: (controller) {
          return Scaffold(
            extendBody: false,
            backgroundColor: Colors.black,
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                controller.translate();
              },
              child: const Icon(Icons.translate),
            ),
            body: SafeArea(
              bottom: true,
              child: SingleChildScrollView(
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(left: 16, top: 32),
                          child: Text(
                            controller.networkLanguage,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 24,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          height: Get.height * 0.3,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 12, vertical: 12),
                          child: TextField(
                            maxLines: null,
                            expands: true,
                            controller: controller.targetController,
                            keyboardType: TextInputType.multiline,
                            cursorColor: Colors.white,
                            textAlignVertical: TextAlignVertical.top,
                            style: const TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide: BorderSide.none),
                                fillColor: Colors.grey.shade800),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 16),
                              child: Text(
                                controller.targetLanguage,
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const LanguageWidget()
                          ],
                        ),
                        Container(
                          width: double.infinity,
                          height: Get.height * 0.3,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 12, vertical: 12),
                          child: TextField(
                            readOnly: true,
                            maxLines: null,
                            controller: controller.networkController,
                            textAlignVertical: TextAlignVertical.top,
                            expands: true,
                            cursorColor: Colors.white,
                            style: const TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide: BorderSide.none),
                                fillColor: Colors.grey.shade800),
                          ),
                        )
                      ],
                    ),
                    const LoadingTranslateWidget()
                  ],
                ),
              ),
            ),
          );
        });
  }
}
