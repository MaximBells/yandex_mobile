import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/main/screens/main_screen.dart';
import 'package:yandex_mobile/components/services/request_service.dart';
import 'package:yandex_mobile/components/services/token_service.dart';
import 'package:yandex_mobile/static/app_string.dart';

class SplashScreenController extends GetxController {
  Future<PermissionRequestResponse> onPermissionRequest(
      InAppWebViewController controller,
      String origin,
      List<String> resources) async {
    return PermissionRequestResponse(
      resources: resources,
      action: PermissionRequestResponseAction.GRANT,
    );
  }

  Future<void> onLoadStart(InAppWebViewController con, Uri? url) async {
    if (url != null && url.toString().contains('verification_code')) {
      await con.stopLoading();
      Get.find<TokenService>().parseAccessToken(url.toString());
      Get.find<RequestService>()
          .getIamToken()
          .then((value) => Get.off(() => const MainScreen()));
    }
  }

  Future<NavigationActionPolicy?> overrideLoading(
      InAppWebViewController controller,
      NavigationAction navigationAction) async {
    if (navigationAction.request.url != null &&
        navigationAction.request.url!.path.contains('verification_code')) {
      Get.find<TokenService>()
          .parseAccessToken(navigationAction.request.url.toString());
      Get.find<RequestService>()
          .getIamToken()
          .then((value) => Get.off(() => const MainScreen()));
      return NavigationActionPolicy.CANCEL;
    }
    return NavigationActionPolicy.ALLOW;
  }

  URLRequest get initialUrlRequest =>
      URLRequest(url: Uri.parse(AppString.initialUrl));
}
