import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/main/screens/main_screen.dart';
import 'package:yandex_mobile/components/screens/error_screen.dart';
import 'package:yandex_mobile/components/screens/splash_screen/splash_screen_controller.dart';
import 'package:yandex_mobile/components/services/request_service.dart';
import 'package:yandex_mobile/components/services/token_service.dart';
import 'package:yandex_mobile/static/app_color.dart';
import 'package:yandex_mobile/static/app_string.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.black,
      body: GetBuilder<SplashScreenController>(
          global: false,
          init: SplashScreenController(),
          builder: (splashController) {
            return InAppWebView(
              androidOnPermissionRequest: splashController.onPermissionRequest,
              onLoadStart: splashController.onLoadStart,
              shouldOverrideUrlLoading: splashController.overrideLoading,
              initialUrlRequest: splashController.initialUrlRequest,
            );
          }),
    );
  }
}
