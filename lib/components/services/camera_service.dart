import 'package:camera/camera.dart';
import 'package:get/get.dart';

class CameraService extends GetxService {
  CameraDescription? _cameraDescription;

  Future<CameraService> init() async {
    // Obtain a list of the available cameras on the device.
    final cameras = await availableCameras();
    // Get a specific camera from the list of available cameras.
    final firstCamera = cameras.first;
    camera = firstCamera;
    return this;
  }

  bool get hasCamera => _cameraDescription != null;

  CameraDescription get camera => _cameraDescription!;

  set camera(CameraDescription value) {
    _cameraDescription = value;
  }
}
