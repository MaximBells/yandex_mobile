import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/components/models/vision_response.dart';
import 'package:yandex_mobile/components/services/token_service.dart';
import 'package:yandex_mobile/static/app_string.dart';
import 'package:yandex_mobile/static/app_url.dart';
import 'package:yandex_mobile/static/utils.dart';

class RequestService extends GetxService {
  final dio = Dio();

  Future<VisionResponse?> visionRequest(String content) async {
    String folderId = AppString.folderId;
    Map body = {
      "folderId": folderId,
      "analyze_specs": [
        {
          "content": content,
          "features": [
            {
              "type": "TEXT_DETECTION",
              "text_detection_config": {
                "language_codes": ["*"]
              }
            }
          ]
        }
      ]
    };
    try {
      var response = await Get.find<RequestService>().dio.post(AppUrl.vision,
          data: jsonEncode(body),
          options: Options(headers: {
            'Authorization': 'Bearer ${Get.find<TokenService>().iamToken}',
            "Content-Type": "application/json",
          }));
      return VisionResponse.fromJson(response.data);
    } on DioException catch (e) {
      logPrint(e.response);
      return null;
    }
  }

  Future<Map<String, dynamic>?> translateRequest(
      String text, String targetLanguage) async {
    String folderId = AppString.folderId;
    Map body = {
      "targetLanguageCode": targetLanguage,
      "texts": [text],
      "folderId": folderId,
    };
    try {
      var response = await Get.find<RequestService>().dio.post(AppUrl.translate,
          data: jsonEncode(body),
          options: Options(headers: {
            'Authorization': 'Bearer ${Get.find<TokenService>().iamToken}',
            "Content-Type": "application/json",
          }));
      return response.data;
    } on DioException catch (e) {
      logPrint(e.response);
      return {};
    }
  }

  Future<void> getIamToken() async {
    try {
      var response = await Get.find<RequestService>().dio.post(AppUrl.token,
          data: jsonEncode({
            "yandexPassportOauthToken": Get.find<TokenService>().accessToken
          }),
          options: Options(headers: {
            "Content-Type": "application/json",
          }));
      if (response.data != null) {
        Get.find<TokenService>().iamToken = response.data['iamToken'];
      }
      logPrint(Get.find<TokenService>().iamToken);
    } on DioException catch (e) {
      logPrint(e.response);
      logPrint(e.message);
      logPrint(e.error);
    }
  }
}
