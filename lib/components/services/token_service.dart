class TokenService {
  String accessToken = "";

  String iamToken = "";

  bool get hasIamToken => iamToken.isNotEmpty;

  bool get hasAccessToken => accessToken.isNotEmpty;

  void parseAccessToken(String value) {
    var subString = value.substring(value.indexOf('#') + 1, value.length);
    accessToken = subString.split('&').first;
    accessToken =
        accessToken.substring(accessToken.indexOf('=') + 1, accessToken.length);
  }


}
