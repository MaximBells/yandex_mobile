import 'dart:convert';
import 'dart:io';

extension StringExtension on String {
  String get content => base64Encode(File(this).readAsBytesSync());
}
