class Word {
  Map boundingBox = {};

  List languages = [];

  String text = "";

  double confidence = 0;

  Word(this.boundingBox, this.languages, this.text, this.confidence);

  factory Word.fromJson(Map<dynamic, dynamic> json) {
    return Word(json['boundingBox'], json['languages'], json['text'],
        double.tryParse(json['confidence'].toString()) ?? 0);
  }
}
