import 'package:yandex_mobile/components/models/word.dart';

class Sentence {
  List<Word> words = [];

  String get text {
    String result = "";
    for (var word in words) {
      result += word.text;
      result += " ";
    }
    return result;
  }

  // factory Sentence.fromJson(Map<String, dynamic> json){
  //   return Sentence(words);
  // }

  Sentence(this.words);
}
