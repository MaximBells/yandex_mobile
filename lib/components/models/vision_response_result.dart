import 'package:yandex_mobile/components/models/sentence.dart';
import 'package:yandex_mobile/components/models/vision_result.dart';

class VisionResponseResult {
  final List<VisionResult> results;

  VisionResponseResult(this.results);

  List<Sentence> get summarySentences {
    List<Sentence> summarySentences = [];
    for (var result in results) {
      for (var page in result.textDetection.pages) {
        summarySentences.addAll(page.sentences);
      }
    }
    return summarySentences;
  }

  factory VisionResponseResult.fromJson(dynamic json) {
    return VisionResponseResult((json['results'] as List)
        .map((e) => VisionResult.fromJson(e))
        .toList());
  }
}
