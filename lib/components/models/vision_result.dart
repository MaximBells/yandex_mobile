import 'package:yandex_mobile/components/models/vision_text_detection.dart';

class VisionResult {
  final VisionTextDetection textDetection;





  VisionResult(this.textDetection);

  factory VisionResult.fromJson(dynamic json) {
    return VisionResult(VisionTextDetection.fromJson(json['textDetection']));
  }
}
