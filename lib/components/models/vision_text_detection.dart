import 'package:yandex_mobile/components/models/vision_page.dart';

class VisionTextDetection {
  final List<VisionPage> pages;

  VisionTextDetection(this.pages);

  factory VisionTextDetection.fromJson(dynamic json) {
    return VisionTextDetection(
        (json['pages'] as List).map((e) => VisionPage.fromJson(e)).toList());
  }
}
