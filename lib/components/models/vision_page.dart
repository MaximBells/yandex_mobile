import 'package:yandex_mobile/components/models/block.dart';
import 'package:yandex_mobile/components/models/sentence.dart';

class VisionPage {
  final List<Block> blocks;

  VisionPage(this.blocks);

  List<Sentence> get sentences =>
      blocks.map((block) => Sentence(block.words)).toList();

  factory VisionPage.fromJson(dynamic json) {
    return VisionPage(
        (json['blocks'] as List).map((e) => Block.fromJson(e)).toList());
  }
}
