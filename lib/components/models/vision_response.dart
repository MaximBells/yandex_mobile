import 'package:yandex_mobile/components/models/block.dart';
import 'package:yandex_mobile/components/models/sentence.dart';
import 'package:yandex_mobile/components/models/vision_response_result.dart';

class VisionResponse {
  final List<Sentence> sentences;

  VisionResponse(this.sentences);

  String get parsedText {
    String result = "";
    for (var sentence in sentences) {
      result += sentence.text;
      result += '\n';
    }
    return result;
  }

  void test(Map<String, dynamic> json) {}

  factory VisionResponse.fromJson(Map<String, dynamic> json) {
    List<VisionResponseResult> results = (json['results'] as List)
        .map((e) => VisionResponseResult.fromJson(e))
        .toList();
    List<Sentence> visionSentences = [];
    for (var result in results) {
      visionSentences.addAll(result.summarySentences);
    }
    return VisionResponse(visionSentences);
  }
}
