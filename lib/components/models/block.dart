import 'package:yandex_mobile/components/models/lines.dart';
import 'package:yandex_mobile/components/models/word.dart';


class Block {
  Map boundingBox = {};

  List<Lines> lines = [];

  Block(this.lines, this.boundingBox);

  List<Word> get words {
    List<Word> result = [];
    for (var line in lines) {
      result.addAll(line.words);
    }
    return result;
  }

  factory Block.fromJson(Map<dynamic, dynamic> json) {
    List linesList = json['lines'];
    List<Lines> lines = [];
    for (var line in linesList) {
      lines.add(Lines.fromJson(line));
    }
    var boundingBox = json['boundingBox'];
    return Block(lines, boundingBox);
  }
}
