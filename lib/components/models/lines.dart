import 'package:yandex_mobile/components/models/word.dart';
import 'package:yandex_mobile/static/utils.dart';

class Lines {
  Map boundingBox = {};

  List<Word> words = [];

  Lines(this.boundingBox, this.words);

  factory Lines.fromJson(Map<dynamic, dynamic> json) => Lines(
      json['boundingBox'],
      (json['words'] as List).map((e) => Word.fromJson(e)).toList());
}
