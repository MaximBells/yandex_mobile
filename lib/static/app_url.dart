class AppUrl {
  static const String token = 'https://iam.api.cloud.yandex.net/iam/v1/tokens';
  static const String translate =
      'https://translate.api.cloud.yandex.net/translate/v2/translate';
  static const String vision =
      'https://vision.api.cloud.yandex.net/vision/v1/batchAnalyze';
}
