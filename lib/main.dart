import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:yandex_mobile/blocks/main/data/main_controller.dart';
import 'package:yandex_mobile/blocks/translate/data/translate_controller.dart';
import 'package:yandex_mobile/blocks/vision/data/vision_controller.dart';
import 'package:yandex_mobile/components/screens/splash_screen/splash_screen.dart';
import 'package:yandex_mobile/components/services/camera_service.dart';
import 'package:yandex_mobile/components/services/request_service.dart';
import 'package:yandex_mobile/components/services/token_service.dart';

Future<void> initServices() async {
  Get.put(RequestService());
  await Get.putAsync(() => CameraService().init());
  Get.put(TokenService());
  Get.put(MainController());
  Get.put(TranslateController());
  Get.put(VisionController());
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();
  runApp(const GetMaterialApp(
    defaultTransition: Transition.cupertino,
    transitionDuration: Duration(milliseconds: 500),
    home: SplashScreen(),
  ));
}
